# emr-handler

## build

In the root folder of the project run:

`python3 setup.py sdist`

## Installation

You can install the `amazon-emr-handler` module using pip simply by copying in the root folder of the project you'd like to use it the `amazon_emr_handler-x.x.x.tar.gz` file generated in the build/ folder after building the project:

`pip install amazon_emr_handler-0.0.1.tar.gz`

## emr

The `emr` module provides a class `Emr` to handle Amazon EMR clusters. The `Emr` class allows you to create EMR clusters, submit Spark steps to the clusters, get step logs, and terminate the clusters.

## Usage

Here's an example of how you can use the `Emr` class to create an EMR cluster and submit a Spark step:

```
from amazon_emr_handler.emr import Emr

aws_access_key_id = "your_aws_access_key_id"
aws_secret_access_key = "your_aws_secret_access_key"
region_name = "your_region_name"
dfs_bucket = "your_dfs_bucket"

emr = Emr(
    aws_access_key_id=aws_access_key_id,
    aws_secret_access_key=aws_secret_access_key,
    region_name=region_name,
    dfs_bucket=dfs_bucket,
)

# Create EMR cluster
emr.create_emr_cluster()

# Submit Spark step
file_path = "path/to/your/spark/step.py"
emr.submit_spark_step(file_path)

# Get step logs
step_id = "your_step_id"
logs = emr.get_step_logs_by_id(step_id)

# Terminate EMR cluster
emr.terminate_cluster()
```
