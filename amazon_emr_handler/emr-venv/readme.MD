# Dockerfile for packaging Python dependencies with venv-pack

This Dockerfile provides an example of how to package Python dependencies with venv-pack. The resulting `pyspark_deps.tar.gz` file can be used with EMR Serverless to run PySpark jobs.

## Usage

To build the Docker image, run the following command in the directory containing the Dockerfile:

`DOCKER_BUILDKIT=1 docker build --output . .`

This command builds the Docker image with BuildKit enabled and generate the `pyspark_deps.tar.gz ` in the same folder
this file needs to be uploaded to S3 in the folder `s3://planetwatch-dfs/venv`
