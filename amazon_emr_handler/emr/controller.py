import os
import time
import awswrangler as wr

from .services import create_custom_cluster, get_emr_step_logs
from .logger import logger


def create_emr_cluster(
    cluster_name: str,
    logging_s3_path: str,
    executor_memory: int,
    executor_cores: int,
    num_executors: int ,
    driver_memory: int,
    bootstrap_paths: list,
    env_vars: dict,
) -> str:
    logger.info(f"Creating EMR cluster: {cluster_name}")
    cluster_id = create_custom_cluster(
        cluster_name,
        bootstrap_paths=bootstrap_paths,
        logging_s3_path=logging_s3_path,
        env_vars=env_vars,
        executor_memory=executor_memory,
        executor_cores=executor_cores,
        num_executors=num_executors,
        driver_memory=driver_memory,
    )
    logger.info(f"Cluster ID: {cluster_id}")

    cluster_state = wr.emr.get_cluster_state(cluster_id)
    logger.info(f"Cluster state: {cluster_state}")
    if cluster_state in ["TERMINATING", "TERMINATED", "TERMINATED_WITH_ERRORS"]:
        logger.warning(
            f"Cluster is in a terminal state ({cluster_state}), cannot submit step."
        )
        # Terminate Cluster
        wr.emr.terminate_cluster(cluster_id)
        # Wait for cluster to terminate
        while wr.emr.get_cluster_state(cluster_id) != "TERMINATED":
            time.sleep(10)
        raise Exception(
            f"Cluster is in a terminal state ({cluster_state}), cannot submit step."
        )

    return cluster_id


def submit_step(
    script_path: str,
    script_name: str,
    cluster_id: str,
    script_args: str ="",
    py_files: str ="",
) -> str:
    '''Submit step to EMR cluster
    '''    
    logger.info(f"Submitting step py_files: {py_files}")
    # spark-submit --class <main-class> --master <master-url> --deploy-mode <deploy-mode> --driver-memory <driver-memory> --executor-memory <executor-memory> --executor-cores <executor-cores> --jars <comma-separated-list-of-jars> <application-jar> <application-arguments>
    step_args = [
        "spark-submit",
        "--verbose",
        "--deploy-mode",
        "client",
        # "cluster",
        # "--master",
        # "yarn",
        "--jars",
        f"s3://planetwatch-dfs/venv/opensearch-hadoop-1.0.1.jar,s3://planetwatch-dfs/venv/aws-java-sdk-1.12.531.jar",
        "--conf",
        "spark.rpc.message.maxSize=1024",
        "--class",
        os.path.basename(script_path).split(".")[0],
        py_files,
        script_path,
        script_args,
    ]
    try:
        step_id = wr.emr.submit_step(
            cluster_id,
            command=" ".join(step_args),
            name=script_name,
            action_on_failure="CANCEL_AND_WAIT",
        )

        return step_id
    except Exception as e:
        if "Missing application resource" in str(e):
            logger.info(f"Error: {str(e)}")
            logger.info(
                f"Please check that the application resource '{script_name}' is present in the location '{script_path}'"
            )
        else:
            raise e


def get_step_logs(client, dfs_bucket: str, cluster_name: str, cluster_id: str, step_id: str) -> str:
    try:
        # Wait Step
        while wr.emr.get_step_state(cluster_id, step_id) not in [
            "COMPLETED",
            "FAILED",
            "CANCELLED",
        ]:
            logger.info(
                f"Step {step_id} state: {wr.emr.get_step_state(cluster_id, step_id)}"
            )
            time.sleep(10)
        # Terminate Cluster
        if wr.emr.get_step_state(cluster_id, step_id) == "COMPLETED":
            logger.info(
                f"Step {step_id} state: {wr.emr.get_step_state(cluster_id, step_id)}"
            )
        elif wr.emr.get_step_state(cluster_id, step_id) in ["FAILED", "CANCELLED"]:
            logger.error(
                f"Step {step_id} state: {wr.emr.get_step_state(cluster_id, step_id)}"
            )
            logger.error(
                f"Step logs: {get_emr_step_logs(client, dfs_bucket, cluster_name, cluster_id, step_id)}"
            )
        else:
            raise Exception(f"Step {step_id} failed")
        logger.info(
            f"Step logs: {get_emr_step_logs(client, dfs_bucket, cluster_name, cluster_id, step_id)}"
        )
    except Exception as e:
        logger.error(f"Failed to submit step: {e}")
        raise e
