import logging 

logging.basicConfig(level=logging.INFO, datefmt="%Y-%m-%d %H:%M:%S")

logger = logging.getLogger(__name__)
