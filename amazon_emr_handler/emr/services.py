import time
import gzip
from botocore.exceptions import ClientError
import awswrangler as wr

from .logger import logger


def push_local_file_to_s3(
    client, local_file_path: str, s3_bucket_name: str, s3_file_path: str
) -> None:
    """
    Push a local file to S3.
    """
    logger.info(f"Uploading {local_file_path} to {s3_bucket_name}/{s3_file_path}")
    client.upload_file(local_file_path, s3_bucket_name, s3_file_path)


def get_emr_step_logs(
    client,
    s3_bucket_name: str,
    cluster_name: str,
    cluster_id: str,
    step_id: str,
    log_type: str = "stdout.gz",
) -> str:
    """
    Get EMR logs.
    """
    logs_folder = f"emr-logs/{cluster_name}/{cluster_id}/steps/{step_id}"
    while True:
        try:
            response = client.list_objects_v2(Bucket=s3_bucket_name, Prefix=logs_folder)
            # check if contains stdout.gz
            if "Contents" in response:
                for obj in response["Contents"]:
                    if obj["Key"].endswith(log_type):
                        # download the file
                        response = client.get_object(
                            Bucket=s3_bucket_name, Key=f"{logs_folder}/{log_type}"
                        )
                        file_content = gzip.decompress(response["Body"].read()).decode(
                            "utf-8"
                        )
                        return str(file_content)
            logger.info(
                f"Waiting for logs to be available on cluster {cluster_id} step {step_id}..."
            )
            time.sleep(20)
        except ClientError as e:
            logger.error(e)
            raise e


def create_custom_cluster(
    cluster_name: str,
    bootstrap_paths: list,
    logging_s3_path: str,
    env_vars: dict,
    executor_memory: int,
    executor_cores: int,
    num_executors: int,
    driver_memory: int,
) -> str:
    # Call the create_cluster function
    python_export = {"PYSPARK_PYTHON": "/usr/local/python3.8.12/bin/python3.8"}
    # merge env_vars with python_export if env_vars is not None
    if env_vars is not None:
        python_export = {**python_export, **env_vars}
    try:
        cluster_id = wr.emr.create_cluster(
            bootstraps_paths=bootstrap_paths,
            subnet_id="subnet-078b609d2c2c861c7",
            cluster_name=cluster_name,
            logging_s3_path=logging_s3_path,
            emr_release="emr-6.12.0",
            emr_ec2_role="EMR_EC2_DefaultRole",
            emr_role="EMR_DefaultRole",
            instance_type_master="m5.2xlarge",
            instance_type_core="m5.2xlarge",
            instance_ebs_size_master=64,
            instance_ebs_size_core=64,
            instance_num_on_demand_master=1,
            instance_num_on_demand_core=9,
            spot_timeout_to_on_demand_master=True,
            spot_timeout_to_on_demand_core=True,
            python3=False,
            spark_glue_catalog=True,
            hive_glue_catalog=True,
            presto_glue_catalog=True,
            applications=[
                "Hadoop",
                "Hive",
                "Hue",
                "JupyterEnterpriseGateway",
                "JupyterHub",
                "Livy",
                "Pig",
                "Spark",
            ],
            security_group_master="sg-01cb53e20a9c20196",
            security_group_slave="sg-0e4a8d4af37dd3b2c",
            key_pair_name="spark-cluster",
            auto_termination_policy={"IdleTimeout": 14400},
            tags=None,
            spark_pyarrow=False,
            # set Classification to spark-log4j2 to enable log4j2 properties
            configurations=[
                {
                    "Classification": "spark-log4j2",
                    "Properties": {
                        "log4j.rootCategory": "WARN, console",
                        "log4j.appender.console": "org.apache.log4j.ConsoleAppender",
                        "log4j.appender.console.target": "System.err",
                        "log4j.appender.console.layout": "org.apache.log4j.PatternLayout",
                        "log4j.appender.console.layout.ConversionPattern": "%d{yyyy-MM-dd HH:mm:ss} %-5p %c{1}:%L - %m%n",
                    },
                },
                {
                    "Classification": "spark-env",
                    "Properties": {},
                    "Configurations": [
                        {
                            "Classification": "export",
                            "Properties": python_export,
                        }
                    ],
                },
                {
                    "Classification": "spark-defaults",
                    "Properties": {
                        "spark.executor.memory": f"{executor_memory}g",
                        "spark.driver.memory": f"{driver_memory}g",
                        "spark.executor.cores": f"{executor_cores}",
                        "spark.executor.instances": f"{num_executors}",
                        "spark.sql.execution.arrow.pyspark.enabled": "true",
                        "spark.sql.execution.arrow.pyspark.fallback.enabled": "true",
                        "spark.scheduler.mode": "FAIR",
                    },
                },
            ],
        )
        return cluster_id
    except Exception as e:
        print(e)
        raise e
