#!/bin/bash
set -e

PYTHON_VERSION=3.8.12
PYTHON_VERSION_SHORT=3.8

# ----------------------------------------------------------------------
#               Replace old OpenSSL and add build utilities
# ----------------------------------------------------------------------
sudo yum -y remove openssl-devel* && \
sudo yum -y install gcc openssl11-devel bzip2-devel libffi-devel tar gzip wget make expat-devel

# ----------------------------------------------------------------------
#                           Install Python
# ----------------------------------------------------------------------
wget https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tgz
tar xzvf Python-${PYTHON_VERSION}.tgz
cd Python-${PYTHON_VERSION}

# ----------------------------------------------------------------------
# Create similar `CONFIG_ARGS` that AL2 Python is built with
# ----------------------------------------------------------------------
./configure --enable-loadable-sqlite-extensions --with-dtrace --with-lto --enable-optimizations --with-system-expat \
    --prefix=/usr/local/python${PYTHON_VERSION}

# ----------------------------------------------------------------------
#               Install into /usr/local/python{PYTHON_VERSION}
# Note that "make install" links /usr/local/python{PYTHON_VERSION}/bin/python3 while "altinstall" does not
# ----------------------------------------------------------------------
sudo make altinstall
sudo ln -sf /usr/local/python${PYTHON_VERSION}/bin/python${PYTHON_VERSION_SHORT} /usr/bin/python3
# ----------------------------------------------------------------------
#                           Upgrade pip
# ----------------------------------------------------------------------
sudo pip3 install --upgrade pip

# ----------------------------------------------------------------------
#              Install additional Python packages
# ----------------------------------------------------------------------
sudo pip3 install pyspark
sudo pip3 install pyspark[sql]
sudo pip3 install pyspark[pandas_on_spark]
sudo pip3 install mlflow
sudo pip3 install boto3 botocore netCDF4 s3fs h5netcdf pygeohash

# ----------------------------------------------------------------------
#           Add the secondary bootstrap action for numpy
# ----------------------------------------------------------------------
cat <<'EOF' > secondary-bootstrap.sh
#!/bin/bash

# Keep checking the status of node provisioning, once it's SUCCESSFUL, run your code

while true; do
    NODEPROVISIONSTATE=$(sed -n '/localInstance [{]/,/[}]/{
                /nodeProvisionCheckinRecord [{]/,/[}]/ {
                /status: / { p }
                /[}]/a
                }
                /[}]/a
                }' /emr/instance-controller/lib/info/job-flow-state.txt | awk ' { print $2 }')

    if [ "$NODEPROVISIONSTATE" == "SUCCESSFUL" ]; then
        sleep 10
        echo "Running my post provision bootstrap"

        sudo pip3 install pandas==1.0.5 
        sudo pip3 install numpy==1.18.5
        sudo pip3 install scipy==1.4.1
        sudo pip3 install xarray==0.16.1

        break
    fi

    sleep 10
done
EOF

# Execute the secondary bootstrap action in the background
sudo bash secondary-bootstrap.sh &

# ----------------------------------------------------------------------
# When your code completes, you can exit successfully
# ----------------------------------------------------------------------
exit 0