import time
import zipfile
import os
import boto3
import botocore
import awswrangler as wr
import shortuuid

from .logger import logger
from .controller import create_emr_cluster, submit_step, get_step_logs
from .static import python_installation, secondary_bootstrap, prefix_python_libraries


# create emr class
class Emr:
    """
    Emr class to handle EMR cluster creation and step submission.
    """

    def __init__(
        self,
        aws_access_key_id: str,
        aws_secret_access_key: str,
        region_name: str,
        dfs_bucket: str,
        cluster_name: str = None,
        cluster_id: str = None,
    ):
        self.s3_client = boto3.client(
            service_name="s3",
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key,
            config=botocore.client.Config(max_pool_connections=50),
            region_name=region_name,
        )
        self.emr_client = boto3.client(
            service_name="emr",
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key,
            config=botocore.client.Config(max_pool_connections=50),
            region_name=region_name,
        )
        self.cluster_name = cluster_name
        self.cluster_id = cluster_id
        self.dfs_bucket = dfs_bucket
        self.session_job_id_map: dict = {}

    def set_cluster_id(self, cluster_id: str) -> None:
        """Set cluster id

        Args:
            cluster_id (str): EMR cluster id
        """
        self.cluster_id = cluster_id

    def get_cluster_state(self) -> str:
        """Get EMR cluster state

        Returns:
            str: EMR cluster state
        """
        return wr.emr.get_cluster_state(self.cluster_id)

    def list_active_clusters(self) -> list:
        """Get all active EMR cluster ids

        Returns:
            list: list of EMR cluster ids
        """
        try:
            # Get the details of all EMR clusters in the account
            response = self.emr_client.list_clusters(
                ClusterStates=["STARTING", "BOOTSTRAPPING", "RUNNING", "WAITING"]
            )

            # Get the number of EMR clusters in the account
            clusters = response["Clusters"]

            # Get the cluster IDs
            cluster_ids = [cluster["Id"] for cluster in clusters]

            return cluster_ids
        except Exception as e:
            logger.error(f"Failed to get all active clusters id: {e}")
            raise e

    def create_emr_cluster(
        self,
        executor_memory: int = 4,
        executor_cores: int = 2,
        num_executors: int = 2,
        driver_memory: int = 4,
        env_vars: dict = None,
        python_libs: list = [],
    ) -> str:
        """Create EMR cluster
        The cluster comes with the following libraries installed:
        - pandas
        - numpy
        - scipy
        - xarray
        - pyspark
        - pyspark[sql]
        - pyspark[pandas_on_spark]
        - mlflow

        Args:
            executor_memory (int, optional): executor memory. Defaults to 4.
            executor_cores (int, optional): executor cores. Defaults to 2.
            num_executors (int, optional): number of executors. Defaults to 2.
            driver_memory (int, optional): driver memory. Defaults to 4.
            env_vars (dict, optional): environment variables to pass to the cluster. Defaults to None.
            python_libs (list, optional): list of python libraries to install. Defaults to [].
        Returns:
            str: EMR cluster id
        """
        try:
            if self.cluster_id:
                logger.info(f"Using existing cluster: {self.cluster_id}")
                cluster_id = self.cluster_id
            else:
                if not self.cluster_name:
                    self.cluster_name = f"planetwatch_emr_{time.strftime('%Y%m%d')}"
                python_libraries = prefix_python_libraries
                for library in python_libs:
                    python_libraries += "\n" + f"sudo pip3 install {library}"
                merged_string = "\n".join(
                    [python_installation, python_libraries, secondary_bootstrap]
                )
                logger.info(f"Pushing requirements.sh")
                logger.debug(f"requirements.sh: {merged_string}")
                self.s3_client.put_object(
                    Bucket=self.dfs_bucket,
                    Key=f"clusters/{self.cluster_name}/venv/requirements.sh",
                    Body=merged_string,
                )

                # Create EMR Cluster
                cluster_id = create_emr_cluster(
                    self.cluster_name,
                    logging_s3_path=f"s3://{self.dfs_bucket}/emr-logs/{self.cluster_name}",
                    bootstrap_paths=[
                        f"s3://{self.dfs_bucket}/clusters/{self.cluster_name}/venv/requirements.sh"
                    ],
                    env_vars=env_vars,
                    executor_memory=executor_memory,
                    executor_cores=executor_cores,
                    num_executors=num_executors,
                    driver_memory=driver_memory,
                )
            while wr.emr.get_cluster_state(cluster_id) not in [
                "WAITING",
                "RUNNING",
            ]:
                time.sleep(10)
                logger.info(
                    f"Waiting for cluster to be ready: {cluster_id} on state {wr.emr.get_cluster_state(cluster_id)}"
                )

            self.cluster_id = cluster_id
            return cluster_id
        except Exception as e:
            logger.error(f"Failed to push local file to s3: {e}")
            raise e

    def start_session(self) -> str:
        """Start EMR job session
        Returns: str: EMR session id
        """
        session = shortuuid.uuid()
        self.session_job_id_map[session] = []
        return session

    def add_file_to_session(self, session: str, local_file_path: str):
        """Upload local session data to S3
        Args:
            session (str): EMR session id
            local_file_path (str): local file path
        """
        self.s3_client.upload_file(
            local_file_path=self.dfs_bucket,
            s3_bucket_name=f"clusters/{self.cluster_name}/{session}/{os.path.basename(local_file_path)}",
            s3_file_path=local_file_path,
        )

    def end_session(self, session: str):
        """End EMR job session
        Args:
            session (str): EMR session id
        """
        wr.s3.delete_objects(
            f"s3://{self.dfs_bucket}/clusters/{self.cluster_name}/{session}"
        )
        self.session_job_id_map.pop(session)

    def submit_spark_step(
        self, file_path: str, session: str, script_args="", py_files=[]
    ) -> str:
        """Submit spark step to EMR cluster

        Args:
            file_path (str): the full path to the spark step file
            script_args (str, optional): arguments to pass to the spark step. Defaults to "".
            py_files (list, optional): list of python files to pass to the spark step. Defaults to [].

        Returns:
            str: EMR step id
        """
        try:
            script_name = os.path.basename(file_path)
            s3_script_key = (
                f"{self.cluster_name}/sessions/{session}/scripts/{script_name}"
            )
            logger.info(f"Pushing local file to s3: {script_name} into {s3_script_key}")
            self.s3_client.upload_file(
                file_path,
                self.dfs_bucket,
                s3_script_key,
            )

            for py_file in py_files:
                logger.info(f"Zipping local folder: {py_file}")
                zip_path = f"{py_file}.zip"
                # create zip file and add folder and all files in the folder to the zip file
                with zipfile.ZipFile(zip_path, "w") as zip_file:
                    for root, dirs, files in os.walk(py_file):
                        for file in files:
                            zip_file.write(
                                os.path.join(root, file),
                                os.path.relpath(
                                    os.path.join(root, file),
                                    os.path.join(py_file, ".."),
                                ),
                            )
                s3_py_file_key = f"clusters/{self.cluster_name}/sessions/{session}/scripts/{os.path.basename(zip_path)}"
                logger.info(
                    f"Pushing local file to s3: {zip_path} into {s3_py_file_key}"
                )
                self.s3_client.upload_file(
                    zip_path,
                    self.dfs_bucket,
                    s3_py_file_key,
                )

        except Exception as e:
            logger.error(f"Failed to push local file to s3: {e}")
            raise e

        try:
            # Submit Step
            if not py_files:
                step_id = submit_step(
                    f"s3://{self.dfs_bucket}/{s3_script_key}",
                    script_name,
                    self.cluster_id,
                    script_args,
                )
            else:
                s3_py_files = [
                    f"s3://{self.dfs_bucket}/clusters/{self.cluster_name}/sessions/{session}/scripts/{os.path.basename(py_file)}.zip"
                    for py_file in py_files
                ]
                py_files_args = ",".join(s3_py_files)
                step_id = submit_step(
                    f"s3://{self.dfs_bucket}/{s3_script_key}",
                    script_name,
                    self.cluster_id,
                    script_args,
                    py_files=f"--py-files {py_files_args}",
                )
                for py_file in py_files:
                    zip_path = f"{py_file}.zip"
                    logger.info(f"Deleting local folder: {zip_path}")
                    os.remove(zip_path)
            self.session_job_id_map[session] = step_id
            # Get Step Logs
            return step_id
        except Exception as e:
            logger.error(f"Failed to submit step: {e}")
            raise e

    def get_step_logs_by_id(self, step_id: str) -> str:
        """Get EMR step logs by step id

        Args:
            step_id (str): EMR step id

        Returns:
            str: EMR step logs
        """
        try:
            return get_step_logs(
                self.s3_client,
                self.dfs_bucket,
                self.cluster_name,
                self.cluster_id,
                step_id,
            )
        except Exception as e:
            logger.error(f"Failed to get step logs: {e}")
            raise e

    def terminate_cluster(self) -> None:
        """Terminate EMR cluster"""
        try:
            logger.info(f"Terminating cluster: {self.cluster_id}")
            wr.emr.terminate_cluster(self.cluster_id)
            # Wait for cluster to terminate
            while wr.emr.get_cluster_state(self.cluster_id) not in [
                "TERMINATED",
                "TERMINATED_WITH_ERRORS",
            ]:
                time.sleep(10)
            logger.info(f"Cluster terminated: {self.cluster_id}")
        except Exception as e:
            logger.error(f"Failed to terminate cluster: {e}")
            raise e
